use std::time::Duration;

use super::WorldClient;
use tarpc::{client, context, tokio_serde::formats::Json};
use tokio::net::ToSocketAddrs;

pub async fn client(name: String, server_addr: impl ToSocketAddrs) -> anyhow::Result<()> {
    let transport = tarpc::serde_transport::tcp::connect(server_addr, Json::default);
    let client = WorldClient::new(client::Config::default(), transport.await?).spawn();
    let hello = async move {
        tokio::select! {
            hello1 = client.hello(context::current(), format!("{}1", name)) =>  hello1,
            hello2 = client.hello(context::current(), format!("{}2", name)) =>  hello2
        }
    }
    .await;
    println!("{:?}", hello);
    tokio::time::sleep(Duration::from_micros(1)).await;
    Ok(())
}
