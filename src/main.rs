use std::{env, net::{IpAddr, Ipv6Addr, SocketAddr}};

mod client;
mod server;
use client::client;
use server::server;

#[tarpc::service]
pub trait World {
    async fn hello(name: String) -> String;
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let Some(arg) = env::args().nth(1) else {
        eprintln!("input arg server or client");
        return Ok(());
    };
     //let server_addr = SocketAddr::from((IpAddr::V6(Ipv6Addr::LOCALHOST), 8080));
     let server_addr = SocketAddr::from((IpAddr::V6(Ipv6Addr::LOCALHOST), 8080));

    match &arg[..] {
        "server" => server(server_addr).await,
        "client" => client("client".to_string(), server_addr).await,
        _ => Ok(())
    }
}
